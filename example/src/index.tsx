import InlineRulesExample from "InlineRulesExample";
import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import ExternalResolverExample from "./ExternalResolverExample";
import ResolverExample from "ResolverExample";

const examples = [<ExternalResolverExample />, <InlineRulesExample />, <ResolverExample />];
ReactDOM.render(examples, document.getElementById("root"));
