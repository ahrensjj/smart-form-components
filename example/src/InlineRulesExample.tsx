import React from 'react'
import { SubmitHandler } from 'react-hook-form';
import { Input, DateInput, Form, Select } from 'smart-form-components'

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const tomorrow = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));

const onSubmit: SubmitHandler<FormData> = (data) => console.log('Submit:', data);

interface FormData {
  name: string,
  email: string,
  date: Date,
  select: "A" | "B" | "C"
}

const InlineRulesExample = () => {

  return (
    <Form onSubmit={onSubmit}>
      <Input type="text" name="name" rules={{ required: 'name is required' }} />
      <Input type="email" name="email" rules={{
        required: 'email is required',
        pattern: { value: emailRegex, message: 'not a valid email' },

      }} />
      <DateInput name="date" defaultValue={tomorrow} rules={{
        required: 'date is required',
        validate: (date: Date) => (new Date()) < date ? 'date must be in the future' : true
      }} />
      <Select name="select" defaultValue="C">
        <option>A</option>
        <option>B</option>
        <option>C</option>
      </Select>
      <button type={'submit'}>submit</button>
    </Form>
  )
}
export default InlineRulesExample