import React from 'react'

import { Input, DateInput, Form, Select } from 'smart-form-components'
import { Resolver, SubmitHandler } from 'react-hook-form';

const onSubmit: SubmitHandler<FormData> = (data) => console.log('Submit:', data);

interface FormData {
  name: string,
  email: string,
  date: Date,
  select: "A" | "B" | "C"
}

const resolver: Resolver<FormData> = async (values) => {
  const { name, email, date } = values;
  let errors = !name ? {
    name: {
      type: 'required',
      message: 'Name is required',
    },
  } : name.length < 3 ? {
    name: {
      type: 'min',
      message: 'Name is too short'
    }
  } : {};

  errors = {
    ...errors,
    ...(!date ? {
      date: {
        type: 'required', message: 'Date is required'
      }
    } : (new Date()) < date ? {
      date: {
        type: 'min',
        message: 'Date must be in the future'
      }
    } : {})
  }

  errors = {
    ...errors, ...(!email ? {
      email: {
        type: 'required',
        message: 'Email is required'
      }
    } : {})
  };

  return {
    values: errors ? {} : values,
    errors: errors
  }
};

const ResolverExample = () => {

  return (
    <Form onSubmit={onSubmit} options={{ resolver, defaultValues: { select: 'C' } }}>
      <Input type="text" name="name" />
      <Input type="email" name="email" />
      <DateInput name="date" />
      <Select name="select">
        <option>A</option>
        <option>B</option>
        <option>C</option>
      </Select>
      <button type={'submit'}>submit</button>
    </Form>
  )
}
export default ResolverExample