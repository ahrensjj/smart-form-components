import React from 'react'
import { yupResolver } from '@hookform/resolvers/yup';
import { object, string, date } from 'yup'

import { Input, DateInput, Form, Select } from 'smart-form-components'
import { SubmitHandler } from 'react-hook-form';

const tomorrow = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));

interface FormData {
  name: string,
  email: string,
  date: Date,
  select: "A" | "B" | "C"
}

const onSubmit:SubmitHandler<FormData> = data => console.log('Submit:', data)

const resolver = yupResolver(object({
  name: string().required('required'),
  email: string().email().notRequired(),
  date: date().required().min(new Date(), 'must be in the future'),
  select: string().required().notOneOf(['B'], 'B is not allowed')
}));

const ExternalResolverExample = () => {

  return (
    <Form onSubmit={onSubmit} options={{ resolver }}>
      <Input type="text" name="name" />
      <Input type="email" name="email" />
      <DateInput name="date" defaultValue={tomorrow} />
      <Select name="select" defaultValue="C">
        <option>A</option>
        <option>B</option>
        <option>C</option>
      </Select>
      <button type={'submit'}>submit</button>
    </Form>
  )
}
export default ExternalResolverExample