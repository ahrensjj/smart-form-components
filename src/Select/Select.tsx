import * as React from 'react';
import { useFormContext, RegisterOptions } from "react-hook-form";
import ErrorLabel from 'common/ErrorLabel';

export interface SelectProps extends React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement> {
  rules?: RegisterOptions;
  defaultValue?: string | number;
}

export const Select: React.FC<SelectProps> = ({ name = '', rules, defaultValue, ...props }) => {
  const context = useFormContext();
  const isError = () => context?.errors[name];
  const additonalProps = context ? { ref: rules ? context.register(rules) : context.register } : {}
  return (<>
    <select defaultValue={defaultValue} name={name} {...props} {...additonalProps} >
      {props.children}
    </select>
    {isError() && <ErrorLabel name={name} />}
  </>
  );

}
