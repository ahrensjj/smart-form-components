import * as React from 'react';
import { FormProvider, SubmitHandler, useForm, UseFormMethods, UseFormOptions } from 'react-hook-form';

export interface FormProps<T = Record<string, any>> {
  options: UseFormOptions<T, any>,
  useForm?: UseFormMethods<T>,
  onSubmit?: SubmitHandler<T>
}

export const Form: React.FC<FormProps<Record<string, any>> & React.DetailedHTMLProps<React.FormHTMLAttributes<HTMLFormElement>, HTMLFormElement>> = ({ onSubmit, onInvalid, options, ...props }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const methods = props.useForm ? props.useForm : useForm({ ...options });
  const { handleSubmit } = methods;
  const additionProps = onSubmit ? {
    onSubmit: handleSubmit(onSubmit),
  } : {};

  return <FormProvider {...methods} >
    <form {...additionProps}>
      {props.children}
    </form>
  </FormProvider>
}
