import Input from 'Input'
import Form from 'Form'
import Select from 'Select'
import DateInput from 'DateInput'

export type Props = { text: string };

export { Input, Form, Select, DateInput };
