import { Message, ValidationRule, ValidateResult } from "react-hook-form/dist/types/form";

export namespace DateValidation {
    export type DateValidationRules = TypedValidationRules<Date | [Date, Date]>;
    export type TypedValidationRules<T> = Partial<{
        required: Message | ValidationRule<boolean>;
        validate: TypedValidate<T> | Record<string, TypedValidate<T>>;
    }>
    export type TypedValidate<T> = (data: T) => ValidateResult | Promise<ValidateResult>;
    export type DefaultDate = Date | [Date, Date];
}